﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatRoom.Models
{
    public class Message
    {
        public string Id { get; set; }
        public string Content { get; set; }
    }
}