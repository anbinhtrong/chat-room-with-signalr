﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatRoom.Models
{
    public class User
    {
        public string ConnectionId { get; set; }
        public string UserName { get; set; }
    }
}