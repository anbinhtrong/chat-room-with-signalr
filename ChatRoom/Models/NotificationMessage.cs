﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatRoom.Models
{
    public class NotificationMessage
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}