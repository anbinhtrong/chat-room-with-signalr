﻿(function () {
	'uses strict';
	var chatApp = angular.module('ChatApp', ['ngRoute', 'SignalRModule']);
	var registeredFunctions = [];
	var userInfo = {
	    id: '',
        username: ''
	}
    chatApp.config(['$routeProvider', function ($routeProvider) {
	    $routeProvider
			.when('/chat/index', {
			    templateUrl: '/Scripts/views/Chat.html',
			    controller: 'ChatCtrl'
			})
            .when('/chat/login', {
                templateUrl: '/Scripts/views/Login.html',
                controller: 'LoginCtrl'
            })
			.otherwise({
			    redirectTo: 'chat/login'
			});
	}]);

	chatApp.directive('ngEnter', function () {
	    return function (scope, element, attrs) {
	        element.bind("keydown keypress", function (event) {
	            if (event.which === 13) {
	                scope.$apply(function () {
	                    scope.$eval(attrs.ngEnter);
	                });

	                event.preventDefault();
	            }
	        });
	    };
	});

	chatApp.controller("NavigationCtrl", ["$rootScope", "$scope", "$location", "SignalRService", function ($rootScope, $scope, $location,signalRService) {
	    $rootScope.showLogin = false;
	    $scope.logout = function () {
	        signalRService.logout();
	        unsubscribe();
	        $location.path("/chat/login");
	    };

	    function unsubscribe() {
	        for (var i = 0; i < registeredFunctions.length; i++) {
	            registeredFunctions[i]();
	        }
	        registeredFunctions.splice(0, registeredFunctions.length);
	    }
	}]);

    //Message Model: Each message: username, content
	chatApp.controller("LoginCtrl", ["$rootScope", "$scope", "$location", "SignalRService", function ($rootScope, $scope, $location, signalRService) {
        signalRService.connect("chatHub");        
	    $scope.login = function () {
	        signalRService.login($scope.username);	        
	    };

        //-----------Client Callback
	    $scope.$parent.$on('onConnected', function (event, isSuccess) {
	        $rootScope.showLogin = true;
	        console.log(isSuccess);
	        var proxy = signalRService.getProxy();
	        userInfo.username = $scope.username;
	        userInfo.id = proxy.connection.id;
	        $scope.$apply(function () {
	            if (isSuccess) {
	                $location.path("/chat/index");
	            }
	        })	        	        
	    });
    }]);
	chatApp.controller("ChatCtrl", ["$scope", "$location", "SignalRService", function ($scope, $location, signalRService) {
        signalRService.getContactsAndMessages();
        $scope.groups = [];
        $scope.conversation = [];
        $scope.toUserId = {};
        $scope.activeContactId = -1;        
        registerRootEvent();
        $scope.send = function () {
            signalRService.sendPrivateChat($scope.toUserId.id, $scope.message);
            $scope.message = '';
        };

        $scope.viewConversation = function (index) {
            $scope.activeContactId = index;
            $scope.conversation = $scope.contacts[index].messages;
            $scope.toUserId = $scope.contacts[index];
        }
        
        //generate message
        function getMessage(id, message) {
            var name = getUserName(id);
            return {
                username: name,
                content: message
            };
        }

        function getUserName(id) {
            if (userInfo.id == id) {
                return userInfo.username;
            }
            for (var i = 0; i < $scope.contacts.length; i++) {
                if ($scope.contacts[i].id == id) {
                    return $scope.contacts[i].username;
                }
            }
            return "";
        }

        function getIndex(id) {
            for (var i = 0; i < $scope.contacts.length; i++) {
                if ($scope.contacts[i].id == id) {
                    return i;
                }
            }
            return -1;
        };
        
        function registerRootEvent() {
            //---------Client Callback
            var onNewConnectedFn = $scope.$parent.$on('onNewConnected', function (event, id, username) {
                $scope.$apply(function () {
                    $scope.contacts.push({ username: username, id: id, messages: [] });
                })
            });

            registeredFunctions.push(onNewConnectedFn);

            var receivedContactsAndMessagesFn = $scope.$parent.$on("receivedContactsAndMessages", function (event, data) {
                $scope.$apply(function () {
                    console.log(data.contacts);
                    $scope.contacts = data.contacts;                    
                    $scope.groups.push(
                        {
                            name: 'Broadcast',
                            messages: data.messages
                        }
                    );
                    console.log(data.contacts);
                });
            });

            registeredFunctions.push(receivedContactsAndMessagesFn);

            var messageReceivedFn = $scope.$parent.$on("messageReceived", function (event, username, message) {
                $scope.$apply(function () {
                    data.messages.push(getMessage(username, message));
                });
            });

            registeredFunctions.push(messageReceivedFn);

            var sendPrivateMessageFn = $scope.$parent.$on("sendPrivateMessage", function (event, username, message) {
                $scope.$apply(function () {
                    var index = getIndex(username);
                    $scope.contacts[index].messages.push(getMessage(message.id, message.content));
                    console.log(message);
                });
            });

            registeredFunctions.push(sendPrivateMessageFn);

            var onUserDisconnectedFn = $scope.$parent.$on("onUserDisconnected", function (event, userid) {
                $scope.$apply(function () {
                    var index = getIndex(userid);
                    //$scope.contacts[index].messages.push(getMessage(message.id, message.content));
                    $scope.contacts.splice(index, 1);
                    console.log($scope.contacts);
                });
            });

            registeredFunctions.push(onUserDisconnectedFn);
        }        
	}]);	
})();