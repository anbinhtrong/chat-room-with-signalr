﻿(function () {
    'uses strict';
    var chatModule = angular.module('SignalRModule', []);
    chatModule.factory("SignalRService", ["$rootScope", "AutoMapperService", function ($rootScope, autoMapperService) {
        var proxy;
        var connection;        
        return {
            connect: function (hubName) {
                connection = $.hubConnection();
                proxy = connection.createHubProxy(hubName);

                proxy.on('onConnected', function (message) {
                    connection.username = message;
                    $rootScope.$emit("onConnected", message.isSuccess);
                });
                proxy.on('onNewConnected', function (id, username) {
                    $rootScope.$emit("onNewConnected", id, username)
                });
                proxy.on('receivedContactsAndMessages', function (contacts, messages, body) {
                    $rootScope.$emit('receivedContactsAndMessages', autoMapperService.mapContactsAndMessages(contacts, messages));
                });
                proxy.on('messageReceived', function (username, message) {
                    $rootScope.$emit('messageReceived', username, message);
                });
                proxy.on('sendPrivateMessage', function (fromUser, data) {
                    $rootScope.$emit('sendPrivateMessage', fromUser, data);
                }),
                proxy.on('onUserDisconnected', function (userid) {
                    $rootScope.$emit('onUserDisconnected', userid);
                }),
                connection.start().done(function () {
                    console.log("Connection Id: " + connection.id);                    
                });
                
            },
            getUserInfo: function(){
                return {
                    id: connection.id,
                    username: connection.username
                };
            },
            getContact: function(){
                proxy.invoke('')
            },
            getProxy: function(){
                return proxy;
            },
            isConnecting: function () {
                return connection.state === 0;
            },
            isConnected: function () {
                return connection.state === 1;
            },
            connectionState: function () {
                return connection.state;
            },
            login: function (username) {
                proxy.invoke('login', username);
            },
            getContactsAndMessages: function () {
                proxy.invoke('getContactAndMessage');
            },
            sendBroadcast: function (username, message) {
                proxy.invoke('sendBroadcast', username, message);
            },
            sendPrivateChat: function (toUserId, message) {                
                proxy.invoke('sendPrivateChat', toUserId, message);
            },
            logout: function () {                
                connection.stop();
            }
        };
    }]);

    chatModule.factory("AutoMapperService", function () {
        return {
            mapContactsAndMessages: function (contacts, messages) {
                var listContacts = [];
                var listMessages = [];                                
                for (var i = 0; i < contacts.length; i++) {
                    listContacts.push({ id: contacts[i].connectionId, username: contacts[i].userName, messages: [] });
                }
                for (var i = 0; i < messages.length; i++) {
                    listMessages.push({ username: messages[i].userName, message: messages[i].message });
                }
                return { contacts: listContacts, messages: listMessages };
            }
        };
    });
})();