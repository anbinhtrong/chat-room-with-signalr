﻿using System;
namespace ChatRoom.ChatModules
{
    public interface IChatHub
    {
        void GetContactAndMessage();
        void Login(string username);
        void Send(string name, string message);
        void SendPrivateChat(string chatId, string message);
        void SendToAll(string message);
        void Test(string message);
    }
}
