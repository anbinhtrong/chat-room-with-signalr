﻿using ChatRoom.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatRoom.ChatModules
{
    public class ChatHub : Hub, IChatHub
    {
        #region Data members
        static List<User> _connectedUsers = new List<User>();
        static List<Message> _messages = new List<Message>();
        #endregion
        public void Send(string name, string message)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage(name, message);
        }

        public void Login(string username)
        {
            var id = Context.ConnectionId;
            if (_connectedUsers.Count(t => t.ConnectionId == id) == 0)
            {                
                _connectedUsers.Add(new User { ConnectionId = id, UserName = username });
                Clients.Caller.onConnected(new NotificationMessage { IsSuccess = true, Message = username });                
                Clients.AllExcept(id).onNewConnected(id, username);
            }
        }

        public void GetContactAndMessage()
        {
            var connectedUsers = _connectedUsers.Where(t => t.ConnectionId != Context.ConnectionId).ToList();
            Clients.Caller.receivedContactsAndMessages(connectedUsers, _messages);
        }        

        #region Broadcast Message

        public void SendToAll(string message)
        {
            var fromUserId = Context.ConnectionId;
            var fromUser = _connectedUsers.FirstOrDefault(x => x.ConnectionId == fromUserId);
            Clients.All.messageReceived(fromUser.UserName, message);
        }

        #endregion

        #region Private chat
        public void SendPrivateChat(string toUserId, string message)
        {
            var fromUserId = Context.ConnectionId;

            var toUser = _connectedUsers.FirstOrDefault(x => x.ConnectionId == toUserId);
            var fromUser = _connectedUsers.FirstOrDefault(x => x.ConnectionId == fromUserId);

            if (toUser != null && fromUser != null)
            {
                // send to 
                Clients.Client(toUserId).sendPrivateMessage(fromUserId, new Message { Id = fromUser.ConnectionId, Content = message });

                // send to caller user
                Clients.Caller.sendPrivateMessage(toUserId, new Message { Id = fromUser.ConnectionId, Content = message });
            }
        }
        #endregion
        public override System.Threading.Tasks.Task OnDisconnected()
        {
            var item = _connectedUsers.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                _connectedUsers.Remove(item);

                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(id);
                Clients.Caller.onUserDisconnected(id);
            }

            return base.OnDisconnected();
        } 
        public void Test(string message)
        {
            Clients.Caller.sendTestMessage(message);
        }        
    }
}